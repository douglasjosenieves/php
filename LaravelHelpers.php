
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;




if (!function_exists('paginate')) {
    function paginate($items, $perPage = 15, $page = null, $options = [])
    {
        $page  = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }}


if (!function_exists('amount')) {

    function amount($monto)
    {

        if (isset($monto)) {
            return number_format($monto, 2, ',', '.');
        }
        return false;
    }
}

if (!function_exists('currency')) {

    function currency($monto)
    {

        if (isset($monto)) {
            return env('CURRENCY') . number_format($monto, 2, ',', '.');
        }
        return false;
    }
}

if (!function_exists('user')) {
    function user($id)
    {

        $user = User::find($id);

        return $user;

    }

}


if (!function_exists('getCategorias')) {
    function getCategorias()
    {
        $categorias = ['' => 'Seleccione', 'ALOJAMIENTO' => 'ALOJAMIENTO', 'COMIDA' => 'COMIDA', 'DIVERSION' => 'DIVERSIÓN', 'ARTE Y CULTURA' => 'ARTE Y CULTURA', 'SERVICIO' => 'SERVICIO'];
        //  $usuarios   = User::all()->sortBy('nombre')->pluck('id_name', 'id')->prepend('Seleccione', '');
        return $categorias;

    }
}


if (!function_exists('getEstCat')) {
    function getEstCat($padre)
    {

        $usuarios = Categoria::where('categoria_padre', $padre)->get()->sortBy('name')->pluck('name', 'id')->prepend('Seleccione', '');
        return $usuarios;

    }
}