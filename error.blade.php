@if ($message = Session::get('success'))
<div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<i class="fa fa-check mx-2"></i>
	<strong>Hola!</strong> {{ session()->get('success') }} 
</div>
@endif


@if ($message = Session::get('error'))
<div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<i class="fa fa-check mx-2"></i>
	<strong>Hola!</strong> {{ session()->get('error') }} 
</div>
@endif


@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-dismissible fade show mb-0" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<i class="fa fa-check mx-2"></i>
	<strong>Hola!</strong> {{ session()->get('warning') }} 
</div>
@endif


@if ($message = Session::get('info'))
<div class="alert alert-info alert-dismissible fade show mb-0" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<i class="fa fa-check mx-2"></i>
	<strong>Hola!</strong> {{ session()->get('info') }} 
</div>
@endif


@if ($errors->any())
@foreach ($errors->all() as $element)
<div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">×</span>
	</button>
	<i class="fa fa-check mx-2"></i>
	<strong>Error!</strong> {{  $element }} 
</div>
@endforeach
@endif

// asi muestra el mensaje return redirect('apuestas/p1')->with('success', 'Sistema de apuestas!');