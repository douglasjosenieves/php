<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- bootstrap -->
  <link rel="stylesheet" href="{{ asset('site/') }}/css/bootstrap.css">
  <!-- fontawesome -->
  <link rel="stylesheet" href="{{ asset('site/') }}/css/all.css">

  <!-- custom -->
  <link rel="stylesheet" href="{{ asset('site/') }}/css/main.css">
  <link rel="stylesheet" href="{{ asset('css/flag-icon.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">

  <style>
    .flag-text { margin-left: 10px; }  
  </style>

  <title>American Time</title>


  <link rel="icon" type="image/png" href="{{ asset('site/') }}/img/favicon.png" />
</head>
<body class="app-body">
 @include('menu' )


 <header class="py-section-2 bg-white mb-5">
  <div class="container">
    <div class="d-flex justify-content-between align-items-center">
      <h5 class="text-primary font-weight-bold mb-0">Perfil de usuario</h5>
      <a href="/" class="btn-backpage">
        <span aria-hidden="true">&times;</span>
      </a>
    </div>
  </div>
</header>

<main class="pb-5">




 <form method="post" action="{{ route('profile.update') }}" enctype="multipart/form-data" autocomplete="off">
  @csrf
  @method('put')

  <div class="container">
    <div class="row">
      <div class="col-lg-4 pr-xl-5">
        <div class="card pt-1 py-lg-3 mb-3 sticky-md-top">
          <ul class="nav vNav flex-lg-column flex-nowrap" style="overflow-x: auto">
            <li class="nav-item flex-grow-1">
              <a class="nav-link vNav__link d-flex flex-column flex-lg-row align-items-center text-center text-lg-left text-truncate pl-lg-4  " href="{{ url('profile') }}"><i class="fa fa-check-circle vNav__icon text-center ml-lg-n1 mr-lg-3"></i>Verificación</a>
            </li>
            <li class="nav-item flex-grow-1">
              <a class="nav-link vNav__link d-flex flex-column flex-lg-row align-items-center text-center text-lg-left text-truncate pl-lg-4 active" href="{{ url('profile-user') }}"><i class="fa fa-user vNav__icon text-center ml-lg-n1 mr-lg-3"></i>Información de usuario</a>
            </li>
            <li class="nav-item flex-grow-1">
              <a class="nav-link vNav__link d-flex flex-column flex-lg-row align-items-center text-center text-lg-left text-truncate pl-lg-4" href="{{ url('profile-phone') }}"><i class="fa fa-phone vNav__icon text-center ml-lg-n1 mr-lg-3"></i>Teléfono</a>
            </li>
            <li class="nav-item flex-grow-1">
              <a class="nav-link vNav__link d-flex flex-column flex-lg-row align-items-center text-center text-lg-left text-truncate pl-lg-4" href="{{ url('profile-password') }}"><i class="fa fa-user-lock vNav__icon text-center ml-lg-n1 mr-lg-3"></i>Contraseña</a>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-lg-8">
        <div class="card card-body mb-4">
          <h6 class="card-title mb-4">Datos personales</h6>
          @if (session('status'))
          <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('status') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          @endif


          @include('partials.error')
          <div class="row">



            <div class="col-6 col-md-6">
             <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
               <label class="form-control-label" for="input-name">{{ __('Primer Nombre') }}</label>
               <input disabled type="text" name="name" id="input-name" class="form-control  form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __(' Primer Nombre') }}" value="{{ old('name', @$model->name) }}" required   >

               @if ($errors->has('name'))
               <span class="invalid-feedback" role="alert">
                 <strong>{{ $errors->first('name') }}</strong>
               </span>
               @endif
             </div>



           </div>
           <div class="col-6 col-md-6">


             <div class="form-group{{ $errors->has('second_name') ? ' has-danger' : '' }}">
               <label class="form-control-label" for="input-second_name">{{ __('Segundo Nombre:') }}</label>
               <input disabled type="text" name="second_name" id="input-second_name" class="form-control  form-control-alternative{{ $errors->has('second_name') ? ' is-invalid' : '' }}" placeholder="{{ __('Segundo Nombre:') }}" value="{{ old('second_name', @$model->second_name) }}" required   >

               @if ($errors->has('second_name'))
               <span class="invalid-feedback" role="alert">
                 <strong>{{ $errors->first('second_name') }}</strong>
               </span>
               @endif
             </div>
           </div>
           <div class="col-6 col-md-6">
            <div class="form-group{{ $errors->has('last_name') ? ' has-danger' : '' }}">
              <label class="form-control-label" for="input-last_name">{{ __('Primer Apellido') }}</label>
              <input disabled type="text" name="last_name" id="input-last_name" class="form-control  form-control-alternative{{ $errors->has('last_name') ? ' is-invalid' : '' }}" placeholder="{{ __('Primer Apellido') }}" value="{{ old('last_name', @$model->last_name) }}" required   >

              @if ($errors->has('last_name'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('last_name') }}</strong>
              </span>
              @endif
            </div>
          </div>
          <div class="col-6 col-md-6">
           <div class="form-group{{ $errors->has('second_last_name') ? ' has-danger' : '' }}">
             <label class="form-control-label" for="input-second_last_name">{{ __('Segundo Apellido:') }}</label>
             <input disabled type="text" name="second_last_name" id="input-second_last_name" class="form-control  form-control-alternative{{ $errors->has('second_last_name') ? ' is-invalid' : '' }}" placeholder="{{ __('Segundo Apellido:') }}" value="{{ old('second_last_name', @$model->second_last_name) }}" required   >

             @if ($errors->has('second_last_name'))
             <span class="invalid-feedback" role="alert">
               <strong>{{ $errors->first('second_last_name') }}</strong>
             </span>
             @endif
           </div>
         </div>

         <div class="col-md-12">
           <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
             <label class="form-control-label" for="input-email">{{ __('Correo electrónico:') }}</label>
             <input disabled type="email" name="email" id="input-email" class="form-control  form-control-alternative{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('Correo electrónico:') }}" value="{{ old('email', @$model->email) }}" required   >

             @if ($errors->has('email'))
             <span class="invalid-feedback" role="alert">
               <strong>{{ $errors->first('email') }}</strong>
             </span>
             @endif
           </div>
         </div>


         <div class="col-6 col-md-3">
           <div class="form-group{{ $errors->has('date_of_birth_day') ? ' has-danger' : '' }}">
             <label class="form-control-label" for="input-date_of_birth_day">{{ __('Fecha de nacimiento:') }}</label>
             <input type="number" name="date_of_birth_day" id="input-date_of_birth_day" class="form-control  form-control-alternative{{ $errors->has('date_of_birth_day') ? ' is-invalid' : '' }}" placeholder="{{ __('Dia') }}" value="{{ old('date_of_birth_day', @$model->date_of_birth_day) }}" required   >

             @if ($errors->has('date_of_birth_day'))
             <span class="invalid-feedback" role="alert">
               <strong>{{ $errors->first('date_of_birth_day') }}</strong>
             </span>
             @endif
           </div>
         </div>
         <div class="col-6 col-md-3">
           <div class="form-group{{ $errors->has('date_of_birth_month') ? ' has-danger' : '' }}">
             <label class="form-control-label" for="input-date_of_birth_month"> Mes:</label>
             <input type="number" name="date_of_birth_month" id="input-date_of_birth_month" class="form-control  form-control-alternative{{ $errors->has('date_of_birth_month') ? ' is-invalid' : '' }}" placeholder="{{ __('Mes:') }}" value="{{ old('date_of_birth_month', @$model->date_of_birth_month) }}" required   >

             @if ($errors->has('date_of_birth_month'))
             <span class="invalid-feedback" role="alert">
               <strong>{{ $errors->first('date_of_birth_month') }}</strong>
             </span>
             @endif
           </div>
         </div>
         <div class="col-12 col-md-3">
          <div class="form-group{{ $errors->has('date_of_birth_year') ? ' has-error' : '' }}">
           <label class="form-control-label" for="input-monto">{{ __('Año:') }}</label>
           {!! Form::select('date_of_birth_year',  getYear(), @$model->date_of_birth_year, ['id' => 'date_of_birth_year', 'class' => 'form-control', 'required' => 'required']) !!}
           <small class="text-danger">{{ $errors->first('date_of_birth_year') }}</small>
         </div>
       </div>







       {{-- IMG ITEMS --}}
       <div class="col-12 col-md-3">
 <label class="form-control-label" for="input-date_of_birth_month"> Avatar:</label>
  <a href="#!" class="avatar-xl mb-2 mb-md-0">

         <input  style="display:none" id="photo_avatar" name="photo_avatar" type="file" accept="image/*" onchange="loadFile1(event)">

         <img  id="photo_avatar_img" class="img-avatar" src="{{(Auth::user()->photo_avatar == null) ?  asset('images/user.png') :   asset('img-profiles/thumbnail/'.Auth::user()->photo_avatar) }}">


         <div id="photo_avatar_over" class="avatar-overlay">
          <svg width="34" height="35" viewBox="0 0 34 35" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M1.15967 17.84V31.68C1.15967 32.7845 2.0551 33.68 3.15967 33.68H30.8397C31.9442 33.68 32.8397 32.7845 32.8397 31.68V17.84" stroke="white" stroke-width="2" stroke-linecap="round"/>
            <path d="M17.0001 21.8V2M17.0001 2L9.08008 9.92M17.0001 2L24.9201 9.92" stroke="white" stroke-width="2" stroke-linecap="round"/>
          </svg>
        </div>


      </a>
    </div>


    {{-- IMG ITEMS --}}




  </div>
  <hr class="my-5">
  <h6 class="card-title mb-4">Dirección</h6>
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label for="country">País de residencia</label>
        <select name="country"></select>
      </div>

    </div>
    <div class="col-md-12">
     <div class="form-group{{ $errors->has('address') ? ' has-danger' : '' }}">
       <label class="form-control-label" for="input-address">{{ __('Dirección:') }}</label>
       <input type="text" name="address" id="input-address" class="form-control  form-control-alternative{{ $errors->has('address') ? ' is-invalid' : '' }}" placeholder="{{ __('Dirección:') }}" value="{{ old('address', @$model->address) }}" required   >

       @if ($errors->has('address'))
       <span class="invalid-feedback" role="alert">
         <strong>{{ $errors->first('address') }}</strong>
       </span>
       @endif
     </div>
   </div>
   <div class="col-md-4">
     <div class="form-group{{ $errors->has('state') ? ' has-danger' : '' }}">
       <label class="form-control-label" for="input-state">{{ __('Estado/Departamento:') }}</label>
       <input type="text" name="state" id="input-state" class="form-control  form-control-alternative{{ $errors->has('state') ? ' is-invalid' : '' }}" placeholder="{{ __('Estado/Departamento:') }}" value="{{ old('state', @$model->state) }}" required   >

       @if ($errors->has('state'))
       <span class="invalid-feedback" role="alert">
         <strong>{{ $errors->first('state') }}</strong>
       </span>
       @endif
     </div>
   </div>
   <div class="col-6 col-md-4">
    <div class="form-group{{ $errors->has('city') ? ' has-danger' : '' }}">
      <label class="form-control-label" for="input-city">{{ __('Ciudad:') }}</label>
      <input type="text" name="city" id="input-city" class="form-control  form-control-alternative{{ $errors->has('city') ? ' is-invalid' : '' }}" placeholder="{{ __('Ciudad:') }}" value="{{ old('city', @$model->city) }}" required   >

      @if ($errors->has('city'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('city') }}</strong>
      </span>
      @endif
    </div>
  </div>
  <div class="col-6 col-md-4">

    <div class="form-group{{ $errors->has('zip') ? ' has-danger' : '' }}">
      <label class="form-control-label" for="input-zip">{{ __('Código postal:') }}</label>
      <input type="text" name="zip" id="input-zip" class="form-control  form-control-alternative{{ $errors->has('zip') ? ' is-invalid' : '' }}" placeholder="{{ __('Código postal:') }}" value="{{ old('zip', @$model->zip) }}" required   >

      @if ($errors->has('zip'))
      <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('zip') }}</strong>
      </span>
      @endif
    </div>
  </div>
</div>
</div>
<div class="text-right">
<input  type="hidden" value="" name="geoip">
  <button  type="submit" class="btn btn-primary px-4">Guardar</button>
</div>
</div>
</div>
</div>
</form>
</main>

@include('partials.footer' )
<script src="{{ asset('js/select2.min.js') }}"></script>
<script>
  
</script>
<script>
  (function($) {
    $(function() {
      var isoCountries = [
      { id: 'AF', text: 'Afghanistan'},
      { id: 'AX', text: 'Aland Islands'},
      { id: 'AL', text: 'Albania'},
      { id: 'DZ', text: 'Algeria'},
      { id: 'AS', text: 'American Samoa'},
      { id: 'AD', text: 'Andorra'},
      { id: 'AO', text: 'Angola'},
      { id: 'AI', text: 'Anguilla'},
      { id: 'AQ', text: 'Antarctica'},
      { id: 'AG', text: 'Antigua And Barbuda'},
      { id: 'AR', text: 'Argentina'},
      { id: 'AM', text: 'Armenia'},
      { id: 'AW', text: 'Aruba'},
      { id: 'AU', text: 'Australia'},
      { id: 'AT', text: 'Austria'},
      { id: 'AZ', text: 'Azerbaijan'},
      { id: 'BS', text: 'Bahamas'},
      { id: 'BH', text: 'Bahrain'},
      { id: 'BD', text: 'Bangladesh'},
      { id: 'BB', text: 'Barbados'},
      { id: 'BY', text: 'Belarus'},
      { id: 'BE', text: 'Belgium'},
      { id: 'BZ', text: 'Belize'},
      { id: 'BJ', text: 'Benin'},
      { id: 'BM', text: 'Bermuda'},
      { id: 'BT', text: 'Bhutan'},
      { id: 'BO', text: 'Bolivia'},
      { id: 'BA', text: 'Bosnia And Herzegovina'},
      { id: 'BW', text: 'Botswana'},
      { id: 'BV', text: 'Bouvet Island'},
      { id: 'BR', text: 'Brazil'},
      { id: 'IO', text: 'British Indian Ocean Territory'},
      { id: 'BN', text: 'Brunei Darussalam'},
      { id: 'BG', text: 'Bulgaria'},
      { id: 'BF', text: 'Burkina Faso'},
      { id: 'BI', text: 'Burundi'},
      { id: 'KH', text: 'Cambodia'},
      { id: 'CM', text: 'Cameroon'},
      { id: 'CA', text: 'Canada'},
      { id: 'CV', text: 'Cape Verde'},
      { id: 'KY', text: 'Cayman Islands'},
      { id: 'CF', text: 'Central African Republic'},
      { id: 'TD', text: 'Chad'},
      { id: 'CL', text: 'Chile'},
      { id: 'CN', text: 'China'},
      { id: 'CX', text: 'Christmas Island'},
      { id: 'CC', text: 'Cocos (Keeling) Islands'},
      { id: 'CO', text: 'Colombia'},
      { id: 'KM', text: 'Comoros'},
      { id: 'CG', text: 'Congo'},
      { id: 'CD', text: 'Congo}, Democratic Republic'},
      { id: 'CK', text: 'Cook Islands'},
      { id: 'CR', text: 'Costa Rica'},
      { id: 'CI', text: 'Cote D\'Ivoire'},
      { id: 'HR', text: 'Croatia'},
      { id: 'CU', text: 'Cuba'},
      { id: 'CY', text: 'Cyprus'},
      { id: 'CZ', text: 'Czech Republic'},
      { id: 'DK', text: 'Denmark'},
      { id: 'DJ', text: 'Djibouti'},
      { id: 'DM', text: 'Dominica'},
      { id: 'DO', text: 'Dominican Republic'},
      { id: 'EC', text: 'Ecuador'},
      { id: 'EG', text: 'Egypt'},
      { id: 'SV', text: 'El Salvador'},
      { id: 'GQ', text: 'Equatorial Guinea'},
      { id: 'ER', text: 'Eritrea'},
      { id: 'EE', text: 'Estonia'},
      { id: 'ET', text: 'Ethiopia'},
      { id: 'FK', text: 'Falkland Islands (Malvinas)'},
      { id: 'FO', text: 'Faroe Islands'},
      { id: 'FJ', text: 'Fiji'},
      { id: 'FI', text: 'Finland'},
      { id: 'FR', text: 'France'},
      { id: 'GF', text: 'French Guiana'},
      { id: 'PF', text: 'French Polynesia'},
      { id: 'TF', text: 'French Southern Territories'},
      { id: 'GA', text: 'Gabon'},
      { id: 'GM', text: 'Gambia'},
      { id: 'GE', text: 'Georgia'},
      { id: 'DE', text: 'Germany'},
      { id: 'GH', text: 'Ghana'},
      { id: 'GI', text: 'Gibraltar'},
      { id: 'GR', text: 'Greece'},
      { id: 'GL', text: 'Greenland'},
      { id: 'GD', text: 'Grenada'},
      { id: 'GP', text: 'Guadeloupe'},
      { id: 'GU', text: 'Guam'},
      { id: 'GT', text: 'Guatemala'},
      { id: 'GG', text: 'Guernsey'},
      { id: 'GN', text: 'Guinea'},
      { id: 'GW', text: 'Guinea-Bissau'},
      { id: 'GY', text: 'Guyana'},
      { id: 'HT', text: 'Haiti'},
      { id: 'HM', text: 'Heard Island & Mcdonald Islands'},
      { id: 'VA', text: 'Holy See (Vatican City State)'},
      { id: 'HN', text: 'Honduras'},
      { id: 'HK', text: 'Hong Kong'},
      { id: 'HU', text: 'Hungary'},
      { id: 'IS', text: 'Iceland'},
      { id: 'IN', text: 'India'},
      { id: 'ID', text: 'Indonesia'},
      { id: 'IR', text: 'Iran}, Islamic Republic Of'},
      { id: 'IQ', text: 'Iraq'},
      { id: 'IE', text: 'Ireland'},
      { id: 'IM', text: 'Isle Of Man'},
      { id: 'IL', text: 'Israel'},
      { id: 'IT', text: 'Italy'},
      { id: 'JM', text: 'Jamaica'},
      { id: 'JP', text: 'Japan'},
      { id: 'JE', text: 'Jersey'},
      { id: 'JO', text: 'Jordan'},
      { id: 'KZ', text: 'Kazakhstan'},
      { id: 'KE', text: 'Kenya'},
      { id: 'KI', text: 'Kiribati'},
      { id: 'KR', text: 'Korea'},
      { id: 'KW', text: 'Kuwait'},
      { id: 'KG', text: 'Kyrgyzstan'},
      { id: 'LA', text: 'Lao People\'s Democratic Republic'},
      { id: 'LV', text: 'Latvia'},
      { id: 'LB', text: 'Lebanon'},
      { id: 'LS', text: 'Lesotho'},
      { id: 'LR', text: 'Liberia'},
      { id: 'LY', text: 'Libyan Arab Jamahiriya'},
      { id: 'LI', text: 'Liechtenstein'},
      { id: 'LT', text: 'Lithuania'},
      { id: 'LU', text: 'Luxembourg'},
      { id: 'MO', text: 'Macao'},
      { id: 'MK', text: 'Macedonia'},
      { id: 'MG', text: 'Madagascar'},
      { id: 'MW', text: 'Malawi'},
      { id: 'MY', text: 'Malaysia'},
      { id: 'MV', text: 'Maldives'},
      { id: 'ML', text: 'Mali'},
      { id: 'MT', text: 'Malta'},
      { id: 'MH', text: 'Marshall Islands'},
      { id: 'MQ', text: 'Martinique'},
      { id: 'MR', text: 'Mauritania'},
      { id: 'MU', text: 'Mauritius'},
      { id: 'YT', text: 'Mayotte'},
      { id: 'MX', text: 'Mexico'},
      { id: 'FM', text: 'Micronesia}, Federated States Of'},
      { id: 'MD', text: 'Moldova'},
      { id: 'MC', text: 'Monaco'},
      { id: 'MN', text: 'Mongolia'},
      { id: 'ME', text: 'Montenegro'},
      { id: 'MS', text: 'Montserrat'},
      { id: 'MA', text: 'Morocco'},
      { id: 'MZ', text: 'Mozambique'},
      { id: 'MM', text: 'Myanmar'},
      { id: 'NA', text: 'Namibia'},
      { id: 'NR', text: 'Nauru'},
      { id: 'NP', text: 'Nepal'},
      { id: 'NL', text: 'Netherlands'},
      { id: 'AN', text: 'Netherlands Antilles'},
      { id: 'NC', text: 'New Caledonia'},
      { id: 'NZ', text: 'New Zealand'},
      { id: 'NI', text: 'Nicaragua'},
      { id: 'NE', text: 'Niger'},
      { id: 'NG', text: 'Nigeria'},
      { id: 'NU', text: 'Niue'},
      { id: 'NF', text: 'Norfolk Island'},
      { id: 'MP', text: 'Northern Mariana Islands'},
      { id: 'NO', text: 'Norway'},
      { id: 'OM', text: 'Oman'},
      { id: 'PK', text: 'Pakistan'},
      { id: 'PW', text: 'Palau'},
      { id: 'PS', text: 'Palestinian Territory}, Occupied'},
      { id: 'PA', text: 'Panama'},
      { id: 'PG', text: 'Papua New Guinea'},
      { id: 'PY', text: 'Paraguay'},
      { id: 'PE', text: 'Peru'},
      { id: 'PH', text: 'Philippines'},
      { id: 'PN', text: 'Pitcairn'},
      { id: 'PL', text: 'Poland'},
      { id: 'PT', text: 'Portugal'},
      { id: 'PR', text: 'Puerto Rico'},
      { id: 'QA', text: 'Qatar'},
      { id: 'RE', text: 'Reunion'},
      { id: 'RO', text: 'Romania'},
      { id: 'RU', text: 'Russian Federation'},
      { id: 'RW', text: 'Rwanda'},
      { id: 'BL', text: 'Saint Barthelemy'},
      { id: 'SH', text: 'Saint Helena'},
      { id: 'KN', text: 'Saint Kitts And Nevis'},
      { id: 'LC', text: 'Saint Lucia'},
      { id: 'MF', text: 'Saint Martin'},
      { id: 'PM', text: 'Saint Pierre And Miquelon'},
      { id: 'VC', text: 'Saint Vincent And Grenadines'},
      { id: 'WS', text: 'Samoa'},
      { id: 'SM', text: 'San Marino'},
      { id: 'ST', text: 'Sao Tome And Principe'},
      { id: 'SA', text: 'Saudi Arabia'},
      { id: 'SN', text: 'Senegal'},
      { id: 'RS', text: 'Serbia'},
      { id: 'SC', text: 'Seychelles'},
      { id: 'SL', text: 'Sierra Leone'},
      { id: 'SG', text: 'Singapore'},
      { id: 'SK', text: 'Slovakia'},
      { id: 'SI', text: 'Slovenia'},
      { id: 'SB', text: 'Solomon Islands'},
      { id: 'SO', text: 'Somalia'},
      { id: 'ZA', text: 'South Africa'},
      { id: 'GS', text: 'South Georgia And Sandwich Isl.'},
      { id: 'ES', text: 'Spain'},
      { id: 'LK', text: 'Sri Lanka'},
      { id: 'SD', text: 'Sudan'},
      { id: 'SR', text: 'Suriname'},
      { id: 'SJ', text: 'Svalbard And Jan Mayen'},
      { id: 'SZ', text: 'Swaziland'},
      { id: 'SE', text: 'Sweden'},
      { id: 'CH', text: 'Switzerland'},
      { id: 'SY', text: 'Syrian Arab Republic'},
      { id: 'TW', text: 'Taiwan'},
      { id: 'TJ', text: 'Tajikistan'},
      { id: 'TZ', text: 'Tanzania'},
      { id: 'TH', text: 'Thailand'},
      { id: 'TL', text: 'Timor-Leste'},
      { id: 'TG', text: 'Togo'},
      { id: 'TK', text: 'Tokelau'},
      { id: 'TO', text: 'Tonga'},
      { id: 'TT', text: 'Trinidad And Tobago'},
      { id: 'TN', text: 'Tunisia'},
      { id: 'TR', text: 'Turkey'},
      { id: 'TM', text: 'Turkmenistan'},
      { id: 'TC', text: 'Turks And Caicos Islands'},
      { id: 'TV', text: 'Tuvalu'},
      { id: 'UG', text: 'Uganda'},
      { id: 'UA', text: 'Ukraine'},
      { id: 'AE', text: 'United Arab Emirates'},
      { id: 'GB', text: 'United Kingdom'},
      { id: 'US', text: 'United States'},
      { id: 'UM', text: 'United States Outlying Islands'},
      { id: 'UY', text: 'Uruguay'},
      { id: 'UZ', text: 'Uzbekistan'},
      { id: 'VU', text: 'Vanuatu'},
      { id: 'VE', text: 'Venezuela'},
      { id: 'VN', text: 'Viet Nam'},
      { id: 'VG', text: 'Virgin Islands}, British'},
      { id: 'VI', text: 'Virgin Islands}, U.S.'},
      { id: 'WF', text: 'Wallis And Futuna'},
      { id: 'EH', text: 'Western Sahara'},
      { id: 'YE', text: 'Yemen'},
      { id: 'ZM', text: 'Zambia'},
      { id: 'ZW', text: 'Zimbabwe'}
      ];

      function formatCountry (country) {
        if (!country.id) { return country.text; }
        var $country = $(
          '<span class="flag-icon flag-icon-'+ country.id.toLowerCase() +' flag-icon-squared"></span>' +
          '<span class="flag-text">'+ country.text+"</span>"
          );
        return $country;
      };

            //Assuming you have a select element with name country
            // e.g. <select name="name"></select>
            
            $("[name='country']").select2({
              placeholder: "Select a country",

              templateResult: formatCountry,
              data: isoCountries
            });  


            var value = '{{@$model->country }}';

    // Set selected 
    $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
                 
 pais = resp.country;
 $("[name='geoip']").val( JSON.stringify(resp));

 
 if (value == '') {

  $("[name='city']").val( resp.city);
  $("[name='zip']").val( resp.postal);
  $("[name='state']").val( resp.region);
  } 

if (value == '') {paiss =  pais  } else {paiss =  value }
  $("[name='country']").val(paiss);

    $("[name='country']").select2({
      placeholder: "Select a country",

      templateResult: formatCountry,
      data: isoCountries
    }).trigger('change');  

               // callback(countryCode);
            });
 
 
   



  });



})(jQuery);   
</script>

<script>
  var loadFile1 = function(event) {
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('photo_avatar_img');
      output.src = reader.result;
  };
  reader.readAsDataURL(event.target.files[0]);
};

$('#photo_avatar_over').click(function(){ $('#photo_avatar').trigger('click'); });

</script>




</body>
</html>
