////////////////////////CARGAR UNA IMAGEN


		///////JAVASCRIPT
		function changePreview(input, errorClass, imageClass) {

		if (input && input[0]) {

		let reader = new FileReader();

		if (input[0].size > 4000000) {

		$(this).val('');



		$(errorClass).removeClass('d-none');;



		return false;

		}



		$(errorClass).fadeOut();



		reader.onload = function (e) {

		$(imageClass).attr('src', e.target.result);

		};



		reader.readAsDataURL(input[0]);

		}

		}



		$("#capture").change(function () {

		changePreview(this.files, '#__capture_img_error', '#__capture_img');



		});

		//////////////////////////HTML

		<div class="col-md-12">

			<div class="d-flex flex-row flex-md-column justify-content-start align-items-md-center text-md-center mt-4 mt-md-0">

				<div class="avatar-square-fluid avatar-background mr-3 mr-md-0 mb-md-3" style="padding: 0; min-height: 238px">

					<img src="/img/landing/selfie-icon-2.svg" alt="Upload selfie" class="img-fluid " style="height: 100%; max-height: 238px" id="__capture_img">



						<div  id="__capture_img_error" class=" d-none text-danger font-weight-bold">La imagen es muy pesada</div>

					</div>

					<div class="flex-grow-1 mt-2">

						<h6 class="font-14">  Seleccionar una foto donde se muestre tu pago</h6>

						@if ($errors->has('capture'))

						<span class="invalid-feedback" role="alert">

							<strong>{{ $errors->first('capture') }}</strong>

						</span>

						@endif

						<div class="font-12 text-muted">Max. 4mb.</div>

						<label class="btn btn-primary btn-sm rounded-0">

							Foto tipo capture

							<input type="file" class="form-control" id="capture" accept="image/*" style="width: 0; height: 0; padding: 0;" value="" name="capture" >



							</label>

						</div>

					</div>

				</div>





				/////CONTROLLER

				if (isset($request->capture)) {

				$path = Storage::disk('public')->putFile(

				'captures',

				$request->capture

				);

				$charge->capture = 'storage/' . $path;

				}



				///REQUEST

				'capture'      => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4000',

				////////////////////////CARGAR UNA IMAGEN FIN
