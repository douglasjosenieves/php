
    $fixed         = 100; // CARRERA MINIMA
    $kilometer     = 4;
    $distance      = 200;
    $distanceValor = 1;
    $discount      = 0; //DESCUENTO SI APLICA:: PORCENTAJE
    $tax           = 0; //IMPUESTO SI APLICA:: PORCENTAJE

    if ($kilometer <= 4) {
        $tdiscount = $fixed - ($fixed * $discount / 100);
        $tTax      = $tdiscount + ($tdiscount * $tax / 100);
        $total     = $tTax;
        return $total;
    } else {
        $costoConsumido = $distance * $distanceValor;
        $tdiscount      = $costoConsumido - ($costoConsumido * $discount / 100);
        $tTax           = $tdiscount + ($tdiscount * $tax / 100);
        $total          = $tTax;
        return $total;
    }