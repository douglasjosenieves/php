<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- bootstrap -->
    <link rel="stylesheet" href="{{ asset('site/') }}/css/bootstrap.css">
    <!-- fontawesome -->
    <link rel="stylesheet" href="{{ asset('site/') }}/css/all.css">

    <!-- custom -->
    <link rel="stylesheet" href="{{ asset('site/') }}/css/main.css">

    <title>American Time</title>

    <link rel="icon" type="image/png" href="{{ asset('site/') }}/img/favicon.png" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.4/css/intlTelInput.css" integrity="sha256-rTKxJIIHupH7lFo30458ner8uoSSRYciA0gttCkw1JE=" crossorigin="anonymous" />

    <style type="text/css" media="screen">
        .iti__flag {background-image: url("https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.4/img/flags.png");}

        @media (-webkit-min-device-pixel-ratio: 2), (min-resolution: 192dpi) {
          .iti__flag {background-image: url("path/to/flags@2x.png");}
      }
  </style>

</head>
<body class="app-body">
   @include('menu' )

   <header class="py-section-2 bg-white mb-5">
    <div class="container">
        <div class="d-flex justify-content-between align-items-center">
            <h5 class="text-primary font-weight-bold mb-0">Perfil de usuario</h5>
            <a href="/" class="btn-backpage">
                <span aria-hidden="true">&times;</span>
            </a>
        </div>
    </div>
</header>

<main class="pb-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 pr-xl-5">
             <div class="card pt-1 py-lg-3 mb-3 sticky-md-top">
                <ul class="nav vNav flex-lg-column flex-nowrap" style="overflow-x: auto">
                    <li class="nav-item flex-grow-1">
                        <a class="nav-link vNav__link d-flex flex-column flex-lg-row align-items-center text-center text-lg-left text-truncate pl-lg-4  " href="{{ url('profile') }}"><i class="fa fa-check-circle vNav__icon text-center ml-lg-n1 mr-lg-3"></i>Verificación</a>
                    </li>
                    <li class="nav-item flex-grow-1">
                        <a class="nav-link vNav__link d-flex flex-column flex-lg-row align-items-center text-center text-lg-left text-truncate pl-lg-4 " href="{{ url('profile-user') }}"><i class="fa fa-user vNav__icon text-center ml-lg-n1 mr-lg-3"></i>Información de usuario</a>
                    </li>
                    <li class="nav-item flex-grow-1">
                        <a class="nav-link vNav__link d-flex flex-column flex-lg-row align-items-center text-center text-lg-left text-truncate pl-lg-4 active" href="{{ url('profile-phone') }}"><i class="fa fa-phone vNav__icon text-center ml-lg-n1 mr-lg-3"></i>Teléfono</a>
                    </li>
                    <li class="nav-item flex-grow-1">
                        <a class="nav-link vNav__link d-flex flex-column flex-lg-row align-items-center text-center text-lg-left text-truncate pl-lg-4" href="{{ url('profile-password') }}"><i class="fa fa-user-lock vNav__icon text-center ml-lg-n1 mr-lg-3"></i>Contraseña</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="card card-body mb-4">
                <h6 class="card-title mb-4">Número de celular</h6>
                @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('status') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif


                @include('partials.error')
                <div class="row">
                    <div class="col-12 col-md-8">

                       @if (Auth::user()->active_phone == true)
                       <div class="alert alert-success "  >

                        <i class="fa fa-check mx-2"></i>
                        Tu número telofónico ha sido verificado exitosamente!
                    </div>
                    @else







                    <div class="form-group">
                       <p id="output">Please enter a valid number below</p>
                       <label for="phone">Número de celular </label>
                       <div class="d-flex flex-nowrap">
                         <input type="tel" id="phone">

                         <button  id="aa" type="button" class="btn btn-outline-info ws-nowrap ml-3" >Verificar número</button>
                     </div>
                 </div>

                 @endif
             </div>
         </div>
     </div>
       {{--  <div class="text-right">
            <button class="btn btn-primary px-4">Guardar</button>
        </div> --}}
    </div>
</div>
</div>

</main>

@include('partials.footer' )


<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.4/js/intlTelInput.js" integrity="sha256-J5cTd51pKg6Pcr2fkY1GNyVn9/nXvogO7WX7z+o2rG8=" crossorigin="anonymous"></script>
<script>




    var input = document.querySelector("#phone"),
    output = document.querySelector("#output");

    var iti = window.intlTelInput(input, {
        initialCountry: "auto",
        geoIpLookup: function(callback) {
            $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
                var countryCode = (resp && resp.country) ? resp.country : "";
                callback(countryCode);
            });
        },
        nationalMode: true,
utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.4/js/utils.js" // just for formatting/placeholders etc
});

    var handleChange = function() {
        var text = (iti.isValidNumber()) ?  iti.getNumber() : "Please enter a number below";
        var textNode = document.createTextNode(text);
        output.innerHTML = "";
        output.appendChild(textNode);
    };

// listen to "keyup", but also "change" to update when the user selects a country
input.addEventListener('change', handleChange);
input.addEventListener('keyup', handleChange);





</script>
<script>



    jQuery(document).ready(function($) {

       phone = $('#phone').val();


       $('#aa').on('click',  function(event) {
           phone = $('#phone').val();
           event.preventDefault();
      // alert( phone);

    

      if ($('#output').text() == 'Please enter a valid number below' || $('#output').text() == 'Please enter a number below' ) {


        alert('Please enter a valid number below');
      //  return false;
    }  
    else {
        $('#telefono').html($('#output').text());
      $('#verifyPhone').modal('show');
      $.get('{{ url('verify-sms/') }}/'+ $('#output').text() , function(data) {
        console.log(data);
    });
  }
});
   });


</script>

<!-- Modal -->
<div class="modal fade" id="verifyPhone" tabindex="-1" role="dialog" aria-labelledby="verifyPhoneTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header px-4">
                <h6 class="modal-title" id="verifyPhoneTitle">Verificar número telefónico</h6>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="bg-gradient-primary text-white text-center py-5">
                <svg width="135" height="94" viewBox="0 0 135 94" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M51.6611 41.4677C48.9655 35.4133 51.6884 28.32 57.7428 25.6243L104.334 4.88078C110.388 2.18517 117.481 4.90804 120.177 10.9625L129.125 31.0605C131.821 37.1149 129.098 44.2083 123.044 46.9039L76.4527 67.6474C70.3983 70.3431 63.3049 67.6202 60.6093 61.5657L51.6611 41.4677Z" fill="#FFE37B"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M105.554 7.62142L58.963 28.365C54.4222 30.3867 52.38 35.7067 54.4017 40.2475L63.35 60.3455C65.3717 64.8864 70.6917 66.9285 75.2325 64.9068L121.823 44.1632C126.364 42.1415 128.406 36.8215 126.385 32.2807L117.436 12.1827C115.415 7.64186 110.095 5.59971 105.554 7.62142ZM57.7428 25.6243C51.6884 28.32 48.9655 35.4133 51.6611 41.4677L60.6093 61.5657C63.3049 67.6202 70.3983 70.3431 76.4527 67.6474L123.044 46.9039C129.098 44.2083 131.821 37.1149 129.125 31.0605L120.177 10.9625C117.481 4.90804 110.388 2.18517 104.334 4.88078L57.7428 25.6243Z" fill="#FFBE1B"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M62.419 34.1252C62.5585 33.3086 63.3335 32.7597 64.1501 32.8991L90.9309 37.4721L105.452 14.5102C105.895 13.81 106.822 13.6014 107.522 14.0442C108.222 14.487 108.431 15.4135 107.988 16.1137L92.3974 40.7659L63.6451 35.8563C62.8285 35.7169 62.2796 34.9419 62.419 34.1252Z" fill="#FFBE1B"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M85.0623 37.049C85.8357 37.3459 86.222 38.2135 85.9251 38.9869L79.59 55.4905C79.2931 56.2639 78.4255 56.6502 77.6521 56.3533C76.8787 56.0564 76.4924 55.1888 76.7893 54.4154L83.1244 37.9118C83.4213 37.1384 84.2889 36.7521 85.0623 37.049Z" fill="#FFBE1B"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M94.5436 32.8276C94.2468 33.601 94.633 34.4687 95.4065 34.7655L111.91 41.1006C112.683 41.3975 113.551 41.0112 113.848 40.2378C114.145 39.4644 113.758 38.5968 112.985 38.2999L96.4816 31.9648C95.7082 31.6679 94.8405 32.0542 94.5436 32.8276Z" fill="#FFBE1B"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M3.8501 59.4707C3.62546 58.9661 3.85237 58.375 4.3569 58.1504L39.0716 42.6944C39.5762 42.4698 40.1673 42.6967 40.3919 43.2012C40.6165 43.7057 40.3896 44.2969 39.8851 44.5215L5.17038 59.9775C4.66584 60.2021 4.07473 59.9752 3.8501 59.4707Z" fill="#6857E5"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M17.8662 66.3659C17.6416 65.8613 17.8685 65.2702 18.373 65.0456L43.9523 53.657C44.4568 53.4323 45.0479 53.6592 45.2726 54.1638C45.4972 54.6683 45.2703 55.2594 44.7658 55.4841L19.1865 66.8727C18.682 67.0973 18.0908 66.8704 17.8662 66.3659Z" fill="#6857E5"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M30.0557 74.0745C29.831 73.57 30.0579 72.9788 30.5625 72.7542L48.8334 64.6195C49.3379 64.3948 49.929 64.6217 50.1537 65.1263C50.3783 65.6308 50.1514 66.2219 49.6469 66.4466L31.3759 74.5813C30.8714 74.8059 30.2803 74.579 30.0557 74.0745Z" fill="#6857E5"/>
                </svg>

                <h6 class="mt-3 mb-0">Hemos enviado un mensaje al número<br><strong><span id="telefono"></span></strong></h6>

            </div>
            <div class="modal-body px-4">
              <form action="{{ url('verify-sms') }}" method="POST" accept-charset="utf-8">
               @csrf
               <div class="form-group">

                 {{--    <label for="verification-code">Ingresa el código que te hemos enviado</label> --}}
                 <div class="d-flex flex-nowrap">



                    <input id="verification-code" name="code" type="text" class="form-control text-center font-weight-bold" style="letter-spacing: 3px;">


                    <button type="submit" class="btn btn-outline-success ws-nowrap ml-3 px-4">Verificar</button>




                </div>
            </div>
        </form>
        {{--    <div class="alert alert-success"><i class="fa fa-check-circle text-white mr-2"></i>¡Tu número telofónico ha sido verificado exitosamente!</div> --}}
    </div>
    <div class="modal-footer ">
        <button type="button" class="btn btn-light btn-block" data-dismiss="modal">Listo</button>
    </div>
</div>
</div>
</div>


</body>
</html>
